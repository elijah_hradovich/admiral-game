#include "Laser.h"

template<> Laser* Ogre::Singleton<Laser>::msSingleton = 0;

Laser::Laser(Ogre::SceneManager *sceneMgr)
	: mFired(0), mLaserSeconds(0.05f), mCurrentLaserSeconds(0), mSceneMgr(sceneMgr), mLaserNode(0)
{
	mLaser = mSceneMgr->createManualObject("LaserObject");

	mLaserNode =  mSceneMgr->getRootSceneNode()->createChildSceneNode("LaserSceneNode");
	mLaserNode->attachObject(mLaser);
}


Laser::~Laser(void)
{
	mLaserNode->detachAllObjects();
	mLaserNode->getParentSceneNode()->removeAndDestroyChild(mLaserNode->getName());
}

Laser& Laser::getSingleton()
{
	return *msSingleton;
}

Laser* Laser::getSingletonPtr()
{
	return msSingleton;
}

void Laser::update(Ogre::Real time)
{
	if (mFired)
	{
		mCurrentLaserSeconds -= time;
		if (mCurrentLaserSeconds <= 0) {
			removeLaser();
		}
	}
}

void Laser::fire(Spaceship *attacker, Spaceship *target) 
{
	if (mCurrentLaserSeconds <= 0) {
		Ogre::Vector3 spaceshipHeight = Ogre::Vector3(0, attacker->getEntity()->getBoundingBox().getSize().y * 0.5, 0);
		mLaser->begin("Admiral/LaserMaterial", Ogre::RenderOperation::OT_LINE_LIST); 
		mLaser->position(spaceshipHeight + attacker->getEntity()->getParentSceneNode()->convertLocalToWorldPosition(attacker->getEntity()->getParentSceneNode()->getPosition())); 
		mLaser->position(spaceshipHeight + target->getEntity()->getParentSceneNode()->convertLocalToWorldPosition(target->getEntity()->getParentSceneNode()->getPosition()));  
		mLaser->end(); 
		mCurrentLaserSeconds = mLaserSeconds;
	}

	mFired = true;
	mLaserNode->setVisible(true);
}

void Laser::removeLaser(void) 
{
	mFired = false;
	mLaser->clear();
	mLaserNode->setVisible(false);
}
