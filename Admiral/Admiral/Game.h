#ifndef GAME_H
#define GAME_H

#include <OgreCamera.h>
#include <OgreEntity.h>
#include <OgreLogManager.h>
#include <OgreRoot.h>
#include <OgreViewport.h>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>
#include <OgreConfigFile.h>

#include <OISEvents.h>
#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>

#include <SdkTrays.h>
#include <SdkCameraMan.h>

#include "Player.h"
#include "Selection.h"
#include "Laser.h"

/// <summary>	Game. </summary>
///
/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
class Game : public Ogre::FrameListener, public Ogre::WindowEventListener, public OIS::KeyListener, public OIS::MouseListener, public OgreBites::SdkTrayListener
{
public:

    /// <summary>	Default constructor. </summary>
    ///
    /// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
    Game(void);

    /// <summary>	Destructor. </summary>
    ///
    /// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
    ~Game(void);

    /// <summary>	Starts game </summary>
    ///
    /// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
    void go(void);

protected:

    /// <summary>	Setups this game. </summary>
    ///
    /// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
    ///
    /// <returns>	true if it succeeds, false if it fails. </returns>
    bool setup();

    /// <summary>	Configures this object. </summary>
    ///
    /// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
    ///
    /// <returns>	true if it succeeds, false if it fails. </returns>
    bool configure(void);

    /// <summary>	Choose scene manager. </summary>
    ///
    /// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
    void chooseSceneManager(void);

    /// <summary>	Creates the camera. </summary>
    ///
    /// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
    void createCamera(void);

    /// <summary>	Creates frame listener. </summary>
    ///
    /// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
    void createFrameListener(void);

    /// <summary>	Creates the scene. </summary>
    ///
    /// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
    void createScene(void);

    /// <summary>	Destroys the scene. </summary>
    ///
    /// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
    void destroyScene(void);

    /// <summary>	Creates the viewports. </summary>
    ///
    /// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
    void createViewports(void);

    /// <summary>	Sets up the resources. </summary>
    ///
    /// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
    void setupResources(void);

    /// <summary>	Creates resource listener. </summary>
    ///
    /// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
    void createResourceListener(void);

    /// <summary>	Loads the resources. </summary>
    ///
    /// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
    void loadResources(void);

	/// <summary>	Updates animation and physics. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <param name="time">	The time. </param>
	void update(Ogre::Real time);

	/// <summary>	Shows the game result. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <param name="win"> 	Name of the winner. </param>
	/// <param name="lose">	Name of the player who losed. </param>
	void showResult(Ogre::String win, Ogre::String lose);

	/// <summary>	Ends a turn. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	void endTurn();

	/// <summary>	Repair spaceship. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	void repairSpaceship();

    /// <summary>	Frame rendering queued. </summary>
    ///
    /// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
    ///
    /// <param name="evt">	Frame event. </param>
    ///
    /// <returns>	true if it succeeds, false if it fails. </returns>
    bool frameRenderingQueued(const Ogre::FrameEvent& evt);

    /// <summary>	Key pressed event handler. </summary>
    ///
    /// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
    ///
    /// <param name="arg">	Key event. </param>
    ///
    /// <returns>	true if it succeeds, false if it fails. </returns>
    bool keyPressed( const OIS::KeyEvent &arg );

    /// <summary>	Key released event handler. </summary>
    ///
    /// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
    ///
    /// <param name="arg">	Key event </param>
    ///
    /// <returns>	true if it succeeds, false if it fails. </returns>
    bool keyReleased( const OIS::KeyEvent &arg );

    /// <summary>	Mouse moved event handler. </summary>
    ///
    /// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
    ///
    /// <param name="arg">	Mouse event. </param>
    ///
    /// <returns>	true if it succeeds, false if it fails. </returns>
    bool mouseMoved( const OIS::MouseEvent &arg );

    /// <summary>	Mouse pressed event handler. </summary>
    ///
    /// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
    ///
    /// <param name="arg">	Mouse event. </param>
    /// <param name="id"> 	The identifier. </param>
    ///
    /// <returns>	true if it succeeds, false if it fails. </returns>
    bool mousePressed( const OIS::MouseEvent &arg, OIS::MouseButtonID id );

    /// <summary>	Mouse released event handler. </summary>
    ///
    /// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
    ///
    /// <param name="arg">	Mouse event. </param>
    /// <param name="id"> 	The identifier. </param>
    ///
    /// <returns>	true if it succeeds, false if it fails. </returns>
    bool mouseReleased( const OIS::MouseEvent &arg, OIS::MouseButtonID id );

	/// <summary>	Button hit event handler. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <param name="button">	[in,out] If non-null, the button. </param>
	void buttonHit(OgreBites::Button* button);

    /// <summary>
    /// Adjust mouse clipping area.
    /// </summary>
    ///
    /// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
    ///
    /// <param name="rw">	[in,out] If non-null, the rw. </param>
    void windowResized(Ogre::RenderWindow* rw);

	/// <summary>	Unattach OIS before window shutdown (very important under Linux) </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <param name="rw">	[in,out] If non-null, the rw. </param>
	void windowClosed(Ogre::RenderWindow* rw);

	Player *mFirstPlayer;
	Player *mSecondPlayer;
	int mPointsPerStep;
	int mCurrentPointsPerStep;

    Ogre::Root *mRoot;
    Ogre::Camera* mCamera;
	Ogre::SceneNode *mCamNode;
	Ogre::SceneNode *mCamPitchNode;
	Ogre::Viewport* mViewport;
    Ogre::SceneManager* mSceneMgr;
    Ogre::RenderWindow* mWindow;
    Ogre::String mResourcesCfg;
    Ogre::String mPluginsCfg;

    // OgreBites
    OgreBites::SdkTrayManager* mTrayMgr;
	OgreBites::ParamsPanel* mResultPanel;
	OgreBites::ParamsPanel* mActivePlayerPanel;
	OgreBites::ParamsPanel* mGameInfoPanel;
	OgreBites::Button* mEndTurnButton;
	OgreBites::Button* mRepairButton;
	Ogre::RaySceneQuery* mRayScnQuery;
    OgreBites::ParamsPanel* mDetailsPanel;     // sample details panel
    bool mCursorWasVisible;                    // was cursor visible before dialog appeared
    bool mShutDown;
	bool mMouseButtonMiddle;
	float mRotateSpeed;						// camera rotate speed
	int mTimeSinceLastFrame;

    //OIS Input devices
    OIS::InputManager* mInputManager;
    OIS::Mouse*    mMouse;
    OIS::Keyboard* mKeyboard;
};

#endif // #ifndef GAME_H