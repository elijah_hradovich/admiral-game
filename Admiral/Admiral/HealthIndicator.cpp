#include "HealthIndicator.h"
#include "Spaceship.h"

HealthIndicator::HealthIndicator(Spaceship *spaceship, Ogre::SceneManager *sceneMgr)
	: mVisible(true),
	mLastHealth(0),
	mLastMaxHealth(0),
	mLastLevel(0),
	mSpaceship(spaceship),
	mHealthIndicatorNode(0),
	mSceneMgr(sceneMgr)
{
	mLastHealth = mSpaceship->getHealth();
	mLastMaxHealth = mSpaceship->getMaxHealth();
	mLastLevel = mSpaceship->getLevel();

	mStar = new Ogre::Image();
	mStar->load("star.png", "Popular");
	mStar->resize(HI_STAR_SIZE, HI_STAR_SIZE);

	createTexture();

	// Create a material using the texture
	material = Ogre::MaterialManager::getSingleton().create(
		"DynamicTextureMaterial_" + mSpaceship->getId(), // name
		Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
 
	material->getTechnique(0)->getPass(0)->createTextureUnitState("HealthIndicatorTexture_" + mSpaceship->getId());
	material->getTechnique(0)->getPass(0)->setSceneBlending(Ogre::SBT_TRANSPARENT_ALPHA);

	mHealthIndicatorSet = mSceneMgr->createBillboardSet("HealthIndicatorSet_" + spaceship->getId());
	mHealthIndicatorSet->setMaterial(material);
	Ogre::Billboard* billboard = mHealthIndicatorSet->createBillboard(Ogre::Vector3(0, 75, 0));
	billboard->setDimensions(HI_WIDTH, HI_HEIGHT);
	
	mHealthIndicatorNode = mSpaceship->getBaseNode()->createChildSceneNode("HealthIndicatorNode+" + spaceship->getId());
	mHealthIndicatorNode->attachObject(mHealthIndicatorSet);
}


HealthIndicator::~HealthIndicator(void)
{
	mHealthIndicatorNode->detachAllObjects();
	mHealthIndicatorNode->getParentSceneNode()->removeAndDestroyChild(mHealthIndicatorNode->getName());
}

void HealthIndicator::createTexture()
{
	Ogre::String textureName = "HealthIndicatorTexture_" + mSpaceship->getId();

	if (Ogre::TextureManager::getSingleton().resourceExists(textureName))
		Ogre::TextureManager::getSingleton().remove(textureName);

	texture = Ogre::TextureManager::getSingleton().createManual(
		textureName, // name
		Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
		Ogre::TEX_TYPE_2D,      // type
		HI_WIDTH, HI_HEIGHT,         // width & height
		0,                // number of mipmaps
		Ogre::PF_BYTE_BGRA,     // pixel format
		Ogre::TU_DEFAULT);      // usage; should be TU_DYNAMIC_WRITE_ONLY_DISCARDABLE for
						  // textures updated very often (e.g. each frame)


	// Get the pixel buffer
	Ogre::HardwarePixelBufferSharedPtr pixelBuffer = texture->getBuffer();
 
	// Lock the pixel buffer and get a pixel box
	pixelBuffer->lock(Ogre::HardwareBuffer::HBL_NORMAL); // for best performance use HBL_DISCARD!
	const Ogre::PixelBox& pixelBox = pixelBuffer->getCurrentLock();
 
	Ogre::uint8* pDest = static_cast<Ogre::uint8*>(pixelBox.data);

	float lifeStatus= (float) mLastHealth / (float) mLastMaxHealth;
	size_t lifeStatusHeight = HI_HEIGHT - 2 * HI_BORDER - HI_TOP_BORDER;
	size_t lifeStatusLength = (size_t) (lifeStatus * (HI_WIDTH - 2* HI_BORDER));

	Ogre::uint8 green;
	Ogre::uint8 red;
	if (lifeStatus > 0.5)
	{
		green = 255;
		red = 0;
	}
	else if (lifeStatus > 0.2)
	{
		green = 255;
		red = 255;
	}
	else
	{
		green = 0;
		red = 255;
	}
 
	for (size_t j = 0; j < HI_HEIGHT; j++)
		for(size_t i = 0; i < HI_WIDTH; i++)
		{
			if (i > (mLastLevel - 1) * HI_STAR_SIZE &&  j < HI_TOP_BORDER)
			{
				*pDest++ = 0; // B
				*pDest++ = 0; // G
				*pDest++ = 0; // R
				*pDest++ = 0; // A
			}
			else if (i < (mLastLevel - 1) * HI_STAR_SIZE &&  j < HI_TOP_BORDER)
			{
				Ogre::ColourValue color = mStar->getColourAt(i % HI_STAR_SIZE, j, 0);
				*pDest++ = static_cast<Ogre::uint8>(color.b * 255); // B
				*pDest++ = static_cast<Ogre::uint8>(color.g * 255); // G
				*pDest++ = static_cast<Ogre::uint8>(color.r * 255); // R
				*pDest++ = static_cast<Ogre::uint8>(color.a * 255); // A
			}
			else if (i < HI_BORDER || i > lifeStatusLength || j < HI_BORDER + HI_TOP_BORDER || j > lifeStatusHeight + HI_TOP_BORDER)
			{
				*pDest++ = 200; // B
				*pDest++ = 200; // G
				*pDest++ = 200; // R
				*pDest++ = 127; // A
			}
			else
			{
				*pDest++ = 0; // B
				*pDest++ = green; // G
				*pDest++ = red; // R
				*pDest++ = 255; // A
			}
		}



	// Unlock the pixel buffer
	pixelBuffer->unlock();
}

void HealthIndicator::update()
{
	if (mSpaceship->getHealth() != mLastHealth || mSpaceship->getMaxHealth() != mLastMaxHealth || mSpaceship->getLevel() != mLastLevel)
	{
		mLastHealth = mSpaceship->getHealth();
		mLastMaxHealth = mSpaceship->getMaxHealth();
		mLastLevel = mSpaceship->getLevel();
		createTexture();
		Ogre::MaterialManager::getSingleton().getByName("DynamicTextureMaterial_" + mSpaceship->getId())->reload();
	}
}