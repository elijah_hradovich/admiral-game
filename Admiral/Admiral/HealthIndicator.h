#ifndef HEALTHINDICATOR_H
#define HEALTHINDICATOR_H

#include <OgreSceneManager.h>
#include <OgreBillboardSet.h>
#include <OgreBillboard.h>
#include <OgreSceneNode.h>
#include <OgreHardwarePixelBuffer.h>

class Spaceship;

/// <summary>	Represents health indicator. </summary>
///
/// <remarks>	Nikagra, 6/4/2013. </remarks>
class HealthIndicator
{
public:
	/// <summary>	Size of the star. </summary>
	static const size_t HI_STAR_SIZE = 10;

	/// <summary>	Width of the indicator. </summary>
	static const size_t HI_WIDTH = 80;

	/// <summary>	Height of the indicator. </summary>
	static const size_t HI_HEIGHT = 20;

	/// <summary>	The top border size. </summary>
	static const size_t HI_TOP_BORDER = HI_STAR_SIZE;

	/// <summary>	The border size. </summary>
	static const size_t HI_BORDER = 2;

	/// <summary>	Constructor. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <param name="spaceship">	[in,out] If non-null, the spaceship. </param>
	/// <param name="sceneMgr"> 	[in,out] If non-null, Ogre scene manager. </param>
	HealthIndicator(Spaceship *spaceship, Ogre::SceneManager *sceneMgr);

	/// <summary>	Destructor. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	~HealthIndicator(void);

	/// <summary>	Updates this object. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	void update();
protected:

	/// <summary>	Creates the texture. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	void createTexture();

	bool mVisible;
	int mLastHealth;
	int mLastMaxHealth;
	int mLastLevel;
	Spaceship *mSpaceship;
	Ogre::BillboardSet *mHealthIndicatorSet;
	Ogre::SceneNode *mHealthIndicatorNode;
	Ogre::TexturePtr texture;
	Ogre::Image *mStar;
	Ogre::MaterialPtr material;
	Ogre::SceneManager *mSceneMgr;
};
#endif // #ifndef HEALTHINDICATOR_H

