#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <vector>
#include <algorithm>

#include <OgreColourValue.h>
#include <OgreSceneManager.h>

#include "Selection.h"

class Spaceship;

/// <summary>	Player. </summary>
///
/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
class Player
{
public:
	/// <summary>	The player color red. </summary>
	static const Ogre::ColourValue Player::COLOR_RED;

	/// <summary>	The player color blue. </summary>
	static const Ogre::ColourValue Player::COLOR_BLUE;

	/// <summary>	The distance beetween ships. </summary>
	static const int PL_SHIP_DISTANCE = 200;

	/// <summary>	The number of ships per player. </summary>
	static const int PL_SHIP_NUMBER = 5;

	/// <summary>	Values that represent QueryFlags. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	enum QueryFlags
	{
		PLAYER1_MASK = 1<<0,
		PLAYER2_MASK = 1<<1
	};

	/// <summary>	Constructor. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <param name="name">			The name. </param>
	/// <param name="color">		The color. </param>
	/// <param name="queryFlag">	The query flag. </param>
	/// <param name="position"> 	The position. </param>
	/// <param name="sceneMgr"> 	[in,out] If non-null, manager for scene. </param>
	Player(std::string name, Ogre::ColourValue color, QueryFlags queryFlag, Ogre::Vector3 position, Ogre::SceneManager *sceneMgr);

	/// <summary>	Destructor. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	~Player(void);

	/// <summary>	Gets player node. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <returns>	null if it fails, else the player node. </returns>
	Ogre::SceneNode* getPlayerNode();

	/// <summary>	Query if this object is active. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <returns>	true if active, false if not. </returns>
	bool isActive();

	/// <summary>	Gets the name. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <returns>	The name. </returns>
	Ogre::String getName();

	/// <summary>	Creates the fleet. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	void createFleet();

	/// <summary>	Spaceship destroyed. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <param name="spaceship">	[in,out] If non-null, the spaceship. </param>
	void spaceshipDestroyed(Spaceship *spaceship);

	/// <summary>	Gets a spaceship by base node. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <param name="baseNode">	[in,out] If non-null, the base node. </param>
	///
	/// <returns>	null if it fails, else the spaceship. </returns>
	Spaceship *getSpaceship(Ogre::SceneNode *baseNode);

protected:
	bool mActive;
	Ogre::String mName;
	Ogre::ColourValue mColor;
	QueryFlags mQueryFlag;
	std::vector<Spaceship*> mFleet;

	Ogre::SceneManager *mSceneMgr;
	Ogre::SceneNode *mPlayerNode;
};
#endif // #define PLAYER_H