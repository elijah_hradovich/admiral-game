#ifndef SELECTION_H
#define SELECTION_H

#include <OgreSceneManager.h>
#include <OgreBillboardSet.h>
#include <OgreBillboard.h>
#include <OgreSceneNode.h>
#include "Spaceship.h"

/// <summary>	Represents spaceship selection. </summary>
///
/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
class Selection : public Ogre::Singleton<Selection>
{
public:

	/// <summary>	Constructor. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <param name="sceneMgr">	[in,out] If non-null, Ogre scene manager to be used. </param>
	Selection(Ogre::SceneManager *sceneMgr);

	/// <summary>	Destructor. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	virtual ~Selection(void);

	/// <summary>	Gets the singleton. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <returns>	The singleton. </returns>
	static Selection& getSingleton();

	/// <summary>	Gets singleton pointer. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <returns>	null if it fails, else the singleton pointer. </returns>
	static Selection* getSingletonPtr();

	/// <summary>	Selects the given spaceship. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <param name="spaceship">	[in,out] If non-null, the spaceship to select. </param>
	void select(Spaceship *spaceship);

	/// <summary>	Diselects currently selected object. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	void diselect();

	/// <summary>	Gets the selected spaceship. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <returns>	null if it fails, else selected spaceship. </returns>
	Spaceship *selected();

protected:
	Spaceship *mSpaceship;
	Ogre::SceneNode *mSelectionNode;
	Ogre::BillboardSet *mSelectionBoardSet;
	Ogre::SceneManager *mSceneMgr;
};
#endif // #ifndef SELECTION_H

