#include <cmath>

#include "Player.h"
#include "Spaceship.h"
#include "HealthIndicator.h"

int Spaceship::nextId = 0;
const float Spaceship::SP_DAMAGE_FACTOR = 1.2f;

Spaceship::Spaceship(Player* owner, Ogre::Vector3 position, int queryFlag, Ogre::SceneManager *sceneMgr)
	: mId(Ogre::StringConverter::toString(nextId++)),
	mHealth(100),
	mMaxHealth(100),
	mDamage(SP_DAMAGE),
	mLevel(1),
	mExperience(0),
	mQueryFlag(queryFlag),
	mOwner(owner), 
	mHealthIndicator(0),
	mSceneMgr(sceneMgr)
{
	mEntity = mSceneMgr->createEntity("Entity_"+mId, "razor.mesh");
	mEntity->setQueryFlags(mQueryFlag);

	// create a particle system with 200 quota, then set its material and dimensions
    Ogre::ParticleSystem* thrusters = mSceneMgr->createParticleSystem(25);
    thrusters->setMaterialName("Examples/Flare");
    thrusters->setDefaultDimensions(25, 25);

		// create two emitters for our thruster particle system
	for (unsigned int i = 0; i < 2; i++)
	{
		Ogre::ParticleEmitter* emitter = thrusters->addEmitter("Point");  // add a point emitter

		// set the emitter properties
		emitter->setAngle(Ogre::Degree(3));
		emitter->setTimeToLive(0.5);
		emitter->setEmissionRate(25);
		emitter->setParticleVelocity(25);
		emitter->setDirection(Ogre::Vector3::NEGATIVE_UNIT_Z);
		emitter->setColour(Ogre::ColourValue::White, Ogre::ColourValue::Red);
		emitter->setPosition(Ogre::Vector3(i == 0 ? 5.7 : -18, 0, 0));
	}

	Ogre::String name = "Base"+mId;
	mBaseNode = mOwner->getPlayerNode()->createChildSceneNode("BaseNode_"+mId, position);

	mMeshNode = mBaseNode->createChildSceneNode("MeshNode_"+mId);
	mMeshNode->attachObject(mEntity);
	mMeshNode->createChildSceneNode(Ogre::Vector3(0, 6.5, -67))->attachObject(thrusters);

	mHealthIndicator = new HealthIndicator(this, mSceneMgr);
}


Spaceship::~Spaceship(void)
{
	mMeshNode->detachAllObjects();
	mOwner->getPlayerNode()->removeAndDestroyChild("BaseNode_"+mId);
	mSceneMgr->destroyEntity(mEntity);
}

void Spaceship::attack(Spaceship *spaceship)
{
	mExperience += (spaceship->mHealth > mDamage) ? mDamage : spaceship->mHealth;

	if (spaceship->mHealth > mDamage)
	{
		spaceship->mHealth -= mDamage;
		spaceship->mHealthIndicator->update();
	} 
	else 
	{
		spaceship->mHealth = 0;
		spaceship->mHealthIndicator->update();

		spaceship->destroy();

		mLevel = (mLevel < SP_MAX_LEVEL) ? mLevel + 1: mLevel;
		mDamage = SP_DAMAGE_FACTOR * mDamage;
		mHealthIndicator->update();
	}
}

void Spaceship::destroy()
{
	Ogre::SceneNode *node = mOwner->getPlayerNode()->createChildSceneNode("Explosion_" + mId);
	node->setPosition(mBaseNode->getPosition());

	Ogre::ParticleSystem *explosion = mSceneMgr->createParticleSystem("Explosions_" + mId, "explosionTemplate");
	explosion->fastForward(1.0);

	node->attachObject(explosion);

	mOwner->spaceshipDestroyed(this);
}

void Spaceship::repair(int hp)
{
	mHealth = (mHealth + hp > mMaxHealth) ? mMaxHealth : mHealth + hp;
	mHealthIndicator->update();
}

Ogre::String Spaceship::getId()
{
	return mId;
}

int Spaceship::getHealth()
{
	return mHealth;
}

int Spaceship::getMaxHealth()
{
	return mMaxHealth;
}

int Spaceship::getLevel()
{
	return mLevel;
}

int Spaceship::getDamage()
{
	return mDamage;
}

int Spaceship::getExperience()
{
	return mExperience;
}

Ogre::SceneNode* Spaceship::getBaseNode()
{
	return mBaseNode;
}

Ogre::Entity *Spaceship::getEntity()
{
	return mEntity;
}