#ifndef STATE_H
#define STATE_H

#include "InputManager.h"

class State : public OIS::MouseListener, public OIS::KeyListener
{
protected:

	virtual bool mouseMoved(const OIS::MouseEvent &arg) = 0;
	virtual bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id) = 0;
	virtual bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id) = 0;

	virtual bool keyPressed(const OIS::KeyEvent &arg) = 0;
	virtual bool keyReleased(const OIS::KeyEvent &arg) = 0;
public:
	virtual ~State() {}
	virtual void enter() = 0;
	virtual void update(Ogre::Real deltaTimeSecs) = 0;
	virtual void exit() = 0;
};
#endif // #ifndef STATE_H
