#ifndef LASER_H
#define LASER_H

#include <OgreSceneManager.h>
#include <OgreSceneNode.h>
#include <OgreManualObject.h>

#include "Spaceship.h"

/// <summary>	Represents Laser instance </summary>
///
/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
class Laser : public Ogre::Singleton<Laser>
{
public:

	/// <summary>	Constructor. </summary>
	///
	/// <remarks>	Nikagra, 6/4/2013. </remarks>
	///
	/// <param name="sceneMgr">	[in,out] If non-null, Ogre scene manager to be used. </param>
	Laser(Ogre::SceneManager *sceneMgr);

	/// <summary>	Destructor. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	virtual ~Laser(void);

	/// <summary>	Gets the singleton. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <returns>	The singleton. </returns>
	static Laser& getSingleton();

	/// <summary>	Gets singleton pointer. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <returns>	null if it fails, else the singleton pointer. </returns>
	static Laser* getSingletonPtr();

	/// <summary>	Updates Laser using given time. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <param name="time">	Time since last update. </param>
	void update(Ogre::Real time);

	/// <summary>	Fires laser </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <param name="attacker">	[in,out] If non-null, the attacker. </param>
	/// <param name="target">  	[in,out] If non-null, attacker target. </param>
	void fire(Spaceship *attacker, Spaceship *target);
protected:
	void removeLaser(void);

	bool mFired;
	Ogre::Real mLaserSeconds;
	Ogre::Real mCurrentLaserSeconds;
	Ogre::ManualObject *mLaser;
	Ogre::SceneNode *mLaserNode;
	Ogre::SceneManager *mSceneMgr;
};
#endif // #ifndef LASER_H

