#include "Selection.h"

template<> Selection* Ogre::Singleton<Selection>::msSingleton = 0;

Selection::Selection(Ogre::SceneManager *sceneMgr)
	: mSpaceship(0), mSceneMgr(sceneMgr), mSelectionNode(0), mSelectionBoardSet(0)
{
	mSelectionBoardSet = mSceneMgr->createBillboardSet("SelectionBoardSet");
	mSelectionBoardSet->setMaterialName("Admiral/Selection");
	mSelectionBoardSet->setBillboardType(Ogre::BillboardType::BBT_PERPENDICULAR_COMMON);
	mSelectionBoardSet->setCommonUpVector(Ogre::Vector3(0,0,1));
	mSelectionBoardSet->setCommonDirection(Ogre::Vector3(0,1,0));
	mSelectionBoardSet->setBillboardOrigin(Ogre::BillboardOrigin::BBO_CENTER);

	Ogre::Billboard* selectionBoard = mSelectionBoardSet->createBillboard(Ogre::Vector3(0, -10.0, 0));
	selectionBoard->setDimensions(250, 250);
}

Selection::~Selection(void)
{
	mSelectionNode->detachAllObjects();
	mSelectionNode->getParentSceneNode()->removeAndDestroyChild(mSelectionNode->getName());
}

Selection& Selection::getSingleton()
{
	return *msSingleton;
}

Selection* Selection::getSingletonPtr()
{
	return msSingleton;
}

void Selection::select(Spaceship *spaceship)
{
	if (spaceship)
	{
		mSpaceship = spaceship;
		mSelectionNode = mSpaceship->getBaseNode()->createChildSceneNode("SelectionNode");
		mSelectionNode->attachObject(mSelectionBoardSet);
		mSelectionNode->setVisible(true);
	}
}

void Selection::diselect()
{
	mSpaceship = 0;
	if (mSelectionNode) 
	{
		mSelectionNode->detachAllObjects();
		mSelectionNode->getParentSceneNode()->removeAndDestroyChild("SelectionNode");
		mSelectionNode = 0;
	}
}

Spaceship *Selection::selected()
{
	return mSpaceship;
}

