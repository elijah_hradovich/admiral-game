#ifndef SPACESHIP_H
#define SPACESHIP_H

#include <string>

#include <OgreSceneManager.h>
#include <OgreParticleSystem.h>
#include <OgreParticleEmitter.h>
#include <OgreEntity.h>

class Player;
class HealthIndicator;

/// <summary>	Spaceship. </summary>
///
/// <remarks>	Nikagra, 6/4/2013. </remarks>
class Spaceship
{
public:
	/// <summary>	Units spaceship get after repairing. </summary>
	static const int SP_REPAIR_UNIT = 10;

	/// <summary>	Maximum spaceship level. </summary>
	static const int SP_MAX_LEVEL = 7;

	/// <summary>	The attack cost in points. </summary>
	static const int SP_ATTACK_COST = 3;

	/// <summary>	Initial spaceship damage. </summary>
	static const int SP_DAMAGE = 8;

	/// <summary>	Damage factor</summary>
	static const float SP_DAMAGE_FACTOR;

	/// <summary>	Constructor. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <param name="owner">		[in,out] If non-null, the owner. </param>
	/// <param name="position"> 	The position. </param>
	/// <param name="queryFlag">	The query flag. </param>
	/// <param name="sceneMgr"> 	[in,out] If non-null, manager for scene. </param>
	Spaceship(Player *owner, Ogre::Vector3 position, int queryFlag, Ogre::SceneManager *sceneMgr);

	/// <summary>	Destructor. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	virtual ~Spaceship(void);

	/// <summary>	Gets the identifier. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <returns>	The identifier. </returns>
	Ogre::String getId();

	/// <summary>	Gets the health. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <returns>	The health. </returns>
	int getHealth();

	/// <summary>	Gets maximum health. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <returns>	The maximum health. </returns>
	int getMaxHealth();

	/// <summary>	Gets the damage. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <returns>	The damage. </returns>
	int getDamage();

	/// <summary>	Gets the experience. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <returns>	The experience. </returns>
	int getExperience();

	/// <summary>	Gets the level. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <returns>	The level. </returns>
	int getLevel();

	/// <summary>	Gets the entity. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <returns>	null if it fails, else the entity. </returns>
	Ogre::Entity *getEntity();

	/// <summary>	Gets base node. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <returns>	null if it fails, else the base node. </returns>
	Ogre::SceneNode* getBaseNode();

	/// <summary>	Attacks the given spaceship. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <param name="spaceship">	[in,out] If non-null, the spaceship. </param>
	void attack(Spaceship *spaceship);

	/// <summary>	Repairs. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <param name="hp">	The hp. </param>
	void repair(int hp);

	/// <summary>	Destroys this object. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	void destroy();

protected:

	/// <summary>	Decrease health. </summary>
	///
	/// <remarks>	Mikita Hradovich, 6/4/2013. </remarks>
	///
	/// <param name="damage">	The damage. </param>
	void decreaseHealth(int damage);

	static int nextId;
	const Ogre::String mId;
	int mHealth;
	int mMaxHealth;
	int mDamage;
	int mLevel;
	int mExperience;
	int mQueryFlag;
	Player *mOwner;
	HealthIndicator *mHealthIndicator;
	Ogre::Entity *mEntity;
	Ogre::SceneNode *mBaseNode;
	Ogre::SceneNode *mMeshNode;
	Ogre::SceneManager *mSceneMgr;
};
#endif // #define SPACESHIP_H

