#include "Spaceship.h"
#include "Player.h"

const Ogre::ColourValue Player::COLOR_RED = Ogre::ColourValue(1.0, 0.0, 0.0);
const Ogre::ColourValue Player::COLOR_BLUE = Ogre::ColourValue(0.0, 1.0, 0.0);

Player::Player(std::string name, Ogre::ColourValue color, QueryFlags queryFlag, Ogre::Vector3 position, Ogre::SceneManager *sceneMgr)
	: mActive(true),
	mName(name),
	mColor(color),
	mQueryFlag(queryFlag),
	mPlayerNode(0),
	mSceneMgr(sceneMgr)
{
	mPlayerNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("PlayerNode_"+mName, position);
}


Player::~Player(void)
{
}

Ogre::SceneNode* Player::getPlayerNode()
{
	return mPlayerNode;
}

void Player::createFleet()
{
	for (int i = 0; i < PL_SHIP_NUMBER; i++)
	{
		mFleet.push_back(new Spaceship(this, Ogre::Vector3((i - (PL_SHIP_NUMBER - 1) / 2.0f) * PL_SHIP_DISTANCE, 0.0, 0.0), (int) mQueryFlag, mSceneMgr));
	}
}

void Player::spaceshipDestroyed(Spaceship *spaceship)
{
	std::vector<Spaceship*>::iterator it = std::find(mFleet.begin(), mFleet.end(), spaceship);
	if (it != mFleet.end())
	{
		if (Selection::getSingletonPtr()->selected() == *it)
			Selection::getSingletonPtr()->diselect();
		mFleet.erase(it);
		delete spaceship;
	}

	if (mFleet.size() == 0)
		mActive = false;
}

Spaceship *Player::getSpaceship(Ogre::SceneNode *baseNode)
{
	std::vector<Spaceship*>::iterator iter = mFleet.begin();
	for (; iter != mFleet.end(); iter++)
	{
		if ((*iter)->getBaseNode() == baseNode)
		{
			return (*iter);
		}
	}

	return 0;
}

Ogre::String Player::getName()
{
	return mName;
}

bool Player::isActive()
{
	return mActive;
}
