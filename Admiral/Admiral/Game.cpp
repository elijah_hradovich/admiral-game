#include "Game.h"

Game::Game(void)
    : mFirstPlayer(0),
	mSecondPlayer(0),
	mPointsPerStep(10),
	mCurrentPointsPerStep(0),
	mRoot(0),
    mCamera(0),
    mSceneMgr(0),
    mWindow(0),
    mResourcesCfg(Ogre::StringUtil::BLANK),
    mPluginsCfg(Ogre::StringUtil::BLANK),
    mTrayMgr(0),
    mMouseButtonMiddle(0),
	mRotateSpeed(0.1f),
	mTimeSinceLastFrame(0),
    mDetailsPanel(0),
    mCursorWasVisible(false),
    mShutDown(false),
    mInputManager(0),
    mMouse(0),
    mKeyboard(0)
{
}

//-------------------------------------------------------------------------------------
Game::~Game(void)
{
    if (mTrayMgr) delete mTrayMgr;
	mTrayMgr = 0;

    //Remove ourself as a Window listener
    Ogre::WindowEventUtilities::removeWindowEventListener(mWindow, this);
    windowClosed(mWindow);
    delete mRoot;
}

//-------------------------------------------------------------------------------------
bool Game::configure(void)
{
    // Show the configuration dialog and initialise the system
    // You can skip this and use root.restoreConfig() to load configuration
    // settings if you were sure there are valid ones saved in ogre.cfg
    if(mRoot->showConfigDialog())
    {
        // If returned true, user clicked OK so initialise
        // Here we choose to let the system create a default rendering window by passing 'true'
        mWindow = mRoot->initialise(true, "Admiral");

        return true;
    }
    else
    {
        return false;
    }
}
//-------------------------------------------------------------------------------------
void Game::chooseSceneManager(void)
{
    // Get the SceneManager, in this case a generic one
    mSceneMgr = mRoot->createSceneManager(Ogre::ST_GENERIC);
}
//-------------------------------------------------------------------------------------
void Game::createCamera(void)
{
	mCamNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("CameraNode");
	mCamPitchNode = mCamNode->createChildSceneNode("CameraPitchNode");
    
    mCamera = mSceneMgr->createCamera("MainCamera");
	mCamera->setNearClipDistance(5);

	mCamNode->setPosition(Ogre::Vector3(-650, 500, -850));
	mCamNode->yaw(Ogre::Radian(6.0 / 5.0 * Ogre::Math::PI));
	mCamPitchNode->pitch(Ogre::Radian(-Ogre::Math::PI / 6));

	mCamPitchNode->attachObject(mCamera);
}
//-------------------------------------------------------------------------------------
void Game::createFrameListener(void)
{
    Ogre::LogManager::getSingletonPtr()->logMessage("*** Initializing OIS ***");
    OIS::ParamList pl;
    size_t windowHnd = 0;
    std::ostringstream windowHndStr;

    mWindow->getCustomAttribute("WINDOW", &windowHnd);
    windowHndStr << windowHnd;
    pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));

    mInputManager = OIS::InputManager::createInputSystem( pl );

    mKeyboard = static_cast<OIS::Keyboard*>(mInputManager->createInputObject( OIS::OISKeyboard, true ));
    mMouse = static_cast<OIS::Mouse*>(mInputManager->createInputObject( OIS::OISMouse, true ));

    mMouse->setEventCallback(this);
    mKeyboard->setEventCallback(this);

    //Set initial mouse clipping size
    windowResized(mWindow);

    //Register as a Window listener
    Ogre::WindowEventUtilities::addWindowEventListener(mWindow, this);

    mTrayMgr = new OgreBites::SdkTrayManager("InterfaceName", mWindow, mMouse, this);
    mTrayMgr->showCursor();

    // create a params panel for displaying sample details
    Ogre::StringVector items;
    items.push_back("cam.pX");
    items.push_back("cam.pY");
    items.push_back("cam.pZ");
    items.push_back("");
    items.push_back("cam.oW");
    items.push_back("cam.oX");
    items.push_back("cam.oY");
    items.push_back("cam.oZ");
    items.push_back("");
    items.push_back("Filtering");
    items.push_back("Poly Mode");

    mDetailsPanel = mTrayMgr->createParamsPanel(OgreBites::TL_NONE, "DetailsPanel", 200, items);
    mDetailsPanel->setParamValue(9, "Bilinear");
    mDetailsPanel->setParamValue(10, "Solid");
    mDetailsPanel->hide();

	Ogre::StringVector playerItems;
    playerItems.push_back("Active player");
	playerItems.push_back("Points left");
    mActivePlayerPanel = mTrayMgr->createParamsPanel(OgreBites::TL_NONE, "PlayerPanel", 200, playerItems);
    mTrayMgr->moveWidgetToTray(mActivePlayerPanel, OgreBites::TL_TOPLEFT, 0);
    mActivePlayerPanel->show();

	Ogre::StringVector gameInfoItems;
    gameInfoItems.push_back("Damage");
	gameInfoItems.push_back("Health");
	gameInfoItems.push_back("Attack price");
	gameInfoItems.push_back("Level");
	gameInfoItems.push_back("Experience");
    mGameInfoPanel = mTrayMgr->createParamsPanel(OgreBites::TL_NONE, "InfoPanel", 200, gameInfoItems);
	mTrayMgr->moveWidgetToTray(mGameInfoPanel, OgreBites::TL_BOTTOMRIGHT, 0);
	mGameInfoPanel->hide();

	mRepairButton = mTrayMgr->createButton(OgreBites::TL_NONE, "RepairButton", "Repair Spaceship", 150);
	mTrayMgr->moveWidgetToTray(mRepairButton, OgreBites::TL_TOPLEFT, 1);
	mRepairButton->show();

	mEndTurnButton = mTrayMgr->createButton(OgreBites::TL_NONE, "EndTurnButton", "End Turn", 150);
	mTrayMgr->moveWidgetToTray(mEndTurnButton, OgreBites::TL_TOPLEFT, 2);
	mEndTurnButton->show();

    mRoot->addFrameListener(this);
}
//-------------------------------------------------------------------------------------
void Game::createScene(void)
{
	// setup some basic lighting for our scene
    mSceneMgr->setAmbientLight(Ogre::ColourValue(0.3, 0.3, 0.3));
    mSceneMgr->createLight()->setPosition(20, 80, 50);
        
    mSceneMgr->setSkyBox(true, "Examples/SpaceSkyBox", 5000);  // set our skybox

	mFirstPlayer = new Player("Player 1", Player::COLOR_BLUE, Player::QueryFlags::PLAYER1_MASK, Ogre::Vector3(0.0, 0.0, -300.0), mSceneMgr);
	mFirstPlayer->createFleet();

	mSecondPlayer = new Player("Player 2", Player::COLOR_RED, Player::QueryFlags::PLAYER2_MASK, Ogre::Vector3(0.0, 0.0, 300.0), mSceneMgr);
	mSecondPlayer->getPlayerNode()->yaw(Ogre::Radian(Ogre::Math::PI));
	mSecondPlayer->createFleet();

	// setup Selection
	new Selection(mSceneMgr);

	// setup Laser
	new Laser(mSceneMgr);
}
//-------------------------------------------------------------------------------------
void Game::destroyScene(void)
{
}
//-------------------------------------------------------------------------------------
void Game::createViewports(void)
{
    // Create one viewport, entire window
    mViewport = mWindow->addViewport(mCamera);
    mViewport->setBackgroundColour(Ogre::ColourValue(0,0,0));

    // Alter the camera aspect ratio to match the viewport
    mCamera->setAspectRatio(
        Ogre::Real(mViewport->getActualWidth()) / Ogre::Real(mViewport->getActualHeight()));
}
//-------------------------------------------------------------------------------------
void Game::setupResources(void)
{
    // Load resource paths from config file
    Ogre::ConfigFile cf;
    cf.load(mResourcesCfg);

    // Go through all sections & settings in the file
    Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

    Ogre::String secName, typeName, archName;
    while (seci.hasMoreElements())
    {
        secName = seci.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i)
        {
            typeName = i->first;
            archName = i->second;
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation(
                archName, typeName, secName);
        }
    }
}
//-------------------------------------------------------------------------------------
void Game::createResourceListener(void)
{
}
//-------------------------------------------------------------------------------------
void Game::loadResources(void)
{
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}
//-------------------------------------------------------------------------------------
void Game::go(void)
{
#ifdef _DEBUG
    mResourcesCfg = "resources_d.cfg";
    mPluginsCfg = "plugins_d.cfg";
#else
    mResourcesCfg = "resources.cfg";
    mPluginsCfg = "plugins.cfg";
#endif

    if (!setup())
        return;

    mRoot->startRendering();

    // clean up
    destroyScene();
}
//-------------------------------------------------------------------------------------
bool Game::setup(void)
{
    mRoot = new Ogre::Root(mPluginsCfg);

    setupResources();

    bool carryOn = configure();
    if (!carryOn) return false;

    chooseSceneManager();
    createCamera();
    createViewports();

    // Set default mipmap level (NB some APIs ignore this)
    Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);

    // Create any resource listeners (for loading screens)
    createResourceListener();
    // Load resources
    loadResources();

    // Create the scene
    createScene();

    createFrameListener();

	mRayScnQuery = mSceneMgr->createRayQuery(Ogre::Ray());

    return true;
};
//-------------------------------------------------------------------------------------
bool Game::frameRenderingQueued(const Ogre::FrameEvent& evt)
{
    if(mWindow->isClosed())
        return false;

    if(mShutDown)
        return false;

	mTimeSinceLastFrame = evt.timeSinceLastFrame;

    //Need to capture/update each device
    mKeyboard->capture();
    mMouse->capture();

    mTrayMgr->frameRenderingQueued(evt);

	// OgreBites problem workaround
	mEndTurnButton->setCaption("End Turn");
	mRepairButton->setCaption("Repair Spaceship");

	update(evt.timeSinceLastFrame);

	if (!mFirstPlayer->isActive()) {
		showResult(mSecondPlayer->getName(), mFirstPlayer->getName());
	} else if (!mSecondPlayer->isActive()) {
		showResult(mFirstPlayer->getName(), mSecondPlayer->getName());
	}

    if (!mTrayMgr->isDialogVisible())
    {
		mActivePlayerPanel->setParamValue(0, mFirstPlayer->getName());
		mActivePlayerPanel->setParamValue(1, Ogre::StringConverter::toString(mPointsPerStep - mCurrentPointsPerStep));

		if (mGameInfoPanel->isVisible())
		{
			Spaceship *s = Selection::getSingleton().selected();
			if (s)
			{
				mGameInfoPanel->setParamValue(0, Ogre::StringConverter::toString(s->getDamage())); // damage
				mGameInfoPanel->setParamValue(1, Ogre::StringConverter::toString(s->getHealth()) + "/" + Ogre::StringConverter::toString(s->getMaxHealth())); // health
				mGameInfoPanel->setParamValue(2, Ogre::StringConverter::toString(Spaceship::SP_ATTACK_COST)); // attack price
				mGameInfoPanel->setParamValue(3, Ogre::StringConverter::toString(s->getLevel())); // level
				mGameInfoPanel->setParamValue(4, Ogre::StringConverter::toString(s->getExperience())); // experience
			}
		}

        if (mDetailsPanel->isVisible())   // if details panel is visible, then update its contents
        {
            mDetailsPanel->setParamValue(0, Ogre::StringConverter::toString(mCamera->getDerivedPosition().x));
            mDetailsPanel->setParamValue(1, Ogre::StringConverter::toString(mCamera->getDerivedPosition().y));
            mDetailsPanel->setParamValue(2, Ogre::StringConverter::toString(mCamera->getDerivedPosition().z));
            mDetailsPanel->setParamValue(4, Ogre::StringConverter::toString(mCamera->getDerivedOrientation().w));
            mDetailsPanel->setParamValue(5, Ogre::StringConverter::toString(mCamera->getDerivedOrientation().x));
            mDetailsPanel->setParamValue(6, Ogre::StringConverter::toString(mCamera->getDerivedOrientation().y));
            mDetailsPanel->setParamValue(7, Ogre::StringConverter::toString(mCamera->getDerivedOrientation().z));
        }
    }

    return true;
}
//-------------------------------------------------------------------------------------
void Game::update(Ogre::Real time)
{
	Laser::getSingleton().update(time);
}
//-------------------------------------------------------------------------------------
void Game::showResult(Ogre::String win, Ogre::String lose)
{
	if (mTrayMgr->getWidget("result") == 0) {
		Ogre::StringVector items2;
		items2.push_back("Result");
		mResultPanel = mTrayMgr->createParamsPanel(OgreBites::TL_NONE, "result", 200, items2);
		mTrayMgr->moveWidgetToTray(mResultPanel, OgreBites::TL_CENTER, 0);
		mResultPanel->setParamValue(0, "'" + win + "' win!");
		mResultPanel->show();
	}
}
//-------------------------------------------------------------------------------------
void Game::endTurn()
{
	Player *p = mFirstPlayer;
	mFirstPlayer = mSecondPlayer;
	mSecondPlayer = p;

	Selection::getSingleton().diselect();
	mCurrentPointsPerStep = 0;
}
//-------------------------------------------------------------------------------------
void Game::repairSpaceship()
{
	Spaceship *s = Selection::getSingleton().selected();
	if (s)
	{
		int pointsLeft = mPointsPerStep - mCurrentPointsPerStep;
		if (pointsLeft >= 5 && s->getHealth() < s->getMaxHealth())
		{
			mCurrentPointsPerStep += 5;
			s->repair(Spaceship::SP_REPAIR_UNIT);
		}
	}
}
//-------------------------------------------------------------------------------------
bool Game::keyPressed( const OIS::KeyEvent &arg )
{
    if (mTrayMgr->isDialogVisible()) return true;   // don't process any more keys if dialog is up

    else if (arg.key == OIS::KC_G)   // toggle visibility of even rarer debugging details
    {
        if (mDetailsPanel->getTrayLocation() == OgreBites::TL_NONE)
        {
            mTrayMgr->moveWidgetToTray(mDetailsPanel, OgreBites::TL_TOPRIGHT, 0);
            //mDetailsPanel->show();
        }
        else
        {
            mTrayMgr->removeWidgetFromTray(mDetailsPanel);
            //mDetailsPanel->hide();
        }
    }
    else if (arg.key == OIS::KC_T)   // cycle polygon rendering mode
    {
        Ogre::String newVal;
        Ogre::TextureFilterOptions tfo;
        unsigned int aniso;

        switch (mDetailsPanel->getParamValue(9).asUTF8()[0])
        {
        case 'B':
            newVal = "Trilinear";
            tfo = Ogre::TFO_TRILINEAR;
            aniso = 1;
            break;
        case 'T':
            newVal = "Anisotropic";
            tfo = Ogre::TFO_ANISOTROPIC;
            aniso = 8;
            break;
        case 'A':
            newVal = "None";
            tfo = Ogre::TFO_NONE;
            aniso = 1;
            break;
        default:
            newVal = "Bilinear";
            tfo = Ogre::TFO_BILINEAR;
            aniso = 1;
        }

        Ogre::MaterialManager::getSingleton().setDefaultTextureFiltering(tfo);
        Ogre::MaterialManager::getSingleton().setDefaultAnisotropy(aniso);
        mDetailsPanel->setParamValue(9, newVal);
    }
    else if(arg.key == OIS::KC_F5)   // refresh all textures
    {
        Ogre::TextureManager::getSingleton().reloadAll();
    }
    else if (arg.key == OIS::KC_SYSRQ)   // take a screenshot
    {
        mWindow->writeContentsToTimestampedFile("screenshot", ".jpg");
    }
    else if (arg.key == OIS::KC_ESCAPE)
    {
        mShutDown = true;
    }

    return true;
}

bool Game::keyReleased( const OIS::KeyEvent &arg )
{
    return true;
}

bool Game::mouseMoved( const OIS::MouseEvent &arg )
{
    if (mTrayMgr->injectMouseMove(arg)) return true;

	if (mMouseButtonMiddle)
	{
		mCamNode->yaw(Ogre::Degree(-arg.state.X.rel * mRotateSpeed));
		mCamPitchNode->pitch(Ogre::Degree(-arg.state.Y.rel * mRotateSpeed));
	}

	Ogre::Quaternion orientation = mCamera->getOrientation();
	
	if (arg.state.X.abs <= 25)
	{
		mCamNode->setPosition(mCamNode->getPosition() + mCamNode->getOrientation() * Ogre::Vector3(-5, 0, 0));
	}

	if (arg.state.Y.abs <= 25)
	{
		mCamNode->setPosition(mCamNode->getPosition() + mCamNode->getOrientation() * Ogre::Vector3(0, 0, -5));
	}

	if (arg.state.X.abs >= mViewport->getActualWidth() - 25)
	{
		mCamNode->setPosition(mCamNode->getPosition() + mCamNode->getOrientation() * Ogre::Vector3(5, 0, 0));
	}

	if (arg.state.Y.abs >= mViewport->getActualHeight() - 25)
	{
		mCamNode->setPosition(mCamNode->getPosition() + mCamNode->getOrientation() * Ogre::Vector3(0, 0, 5));
	}

    return true;
}

bool Game::mousePressed( const OIS::MouseEvent &arg, OIS::MouseButtonID id )
{
	if(mTrayMgr->injectMouseDown(arg, id)) return true;

    if(id == OIS::MB_Left)
	{
		Ogre::Ray mouseRay = mTrayMgr->getCursorRay(mCamera);
 
		mRayScnQuery->setRay(mouseRay);
		mRayScnQuery->setSortByDistance(true);
 
		Ogre::RaySceneQueryResult& result = mRayScnQuery->execute();
		Ogre::RaySceneQueryResult::iterator iter = result.begin();
 
		Selection::getSingletonPtr()->diselect();
		mGameInfoPanel->hide();
		for(iter; iter != result.end(); iter++)
		{
			if(iter->movable && iter->movable->getName().substr(0,7) == "Entity_")
			{
				Ogre::SceneNode *node = iter->movable->getParentSceneNode()->getParentSceneNode();

				Spaceship *s;
				if ((s = mFirstPlayer->getSpaceship(node)) && mPointsPerStep - mCurrentPointsPerStep > Spaceship::SP_ATTACK_COST)
				{
					Selection::getSingletonPtr()->select(s);
					mGameInfoPanel->show();
				}
				break;
			}
		}
	}
	else if(id == OIS::MB_Right)
	{
		Ogre::Ray mouseRay = mTrayMgr->getCursorRay(mCamera);
 
		mRayScnQuery->setRay(mouseRay);
		mRayScnQuery->setSortByDistance(true);
 
		Ogre::RaySceneQueryResult& result = mRayScnQuery->execute();
		Ogre::RaySceneQueryResult::iterator iter = result.begin();
 
		for(iter; iter != result.end(); iter++)
		{
			if(iter->movable && iter->movable->getName().substr(0,7) == "Entity_")
			{
				Ogre::SceneNode *node = iter->movable->getParentSceneNode()->getParentSceneNode();

				Spaceship *s;
				if ((s = mSecondPlayer->getSpaceship(node)) && Selection::getSingletonPtr()->selected() && mPointsPerStep - mCurrentPointsPerStep > Spaceship::SP_ATTACK_COST)
				{
					Laser::getSingleton().fire(Selection::getSingletonPtr()->selected(), s);
					Selection::getSingletonPtr()->selected()->attack(s);
					mCurrentPointsPerStep += Spaceship::SP_ATTACK_COST;
				}
				break;
			}
		}
	}
	else if (id == OIS::MB_Middle) 
	{
		mMouseButtonMiddle = true;
	}

	return true;
}

bool Game::mouseReleased( const OIS::MouseEvent &arg, OIS::MouseButtonID id )
{
	if(mTrayMgr->injectMouseUp(arg, id)) return true;

    if (id == OIS::MB_Middle) 
	{
		mMouseButtonMiddle = false;
	}

    return true;
}

void Game::buttonHit(OgreBites::Button* button)
{
	if(button->getName() == "EndTurnButton")
    {
        endTurn();
    }
	else if (button->getName() == "RepairButton")
	{
		repairSpaceship();
	}
}

//Adjust mouse clipping area
void Game::windowResized(Ogre::RenderWindow* rw)
{
    unsigned int width, height, depth;
    int left, top;
    rw->getMetrics(width, height, depth, left, top);

    const OIS::MouseState &ms = mMouse->getMouseState();
    ms.width = width;
    ms.height = height;
}

//Unattach OIS before window shutdown (very important under Linux)
void Game::windowClosed(Ogre::RenderWindow* rw)
{
    //Only close for window that created OIS (the main window in these demos)
    if( rw == mWindow )
    {
        if( mInputManager )
        {
            mInputManager->destroyInputObject( mMouse );
            mInputManager->destroyInputObject( mKeyboard );

            OIS::InputManager::destroyInputSystem(mInputManager);
            mInputManager = 0;
        }
    }
}